import sys

keys = [
    ["00001", " "],
    ["00010", "e"],
    ["00100", "t"],
    ["01000", "a"],
    ["10000", "o"],
    ["00011", "i"],
    ["00101", "n"],
    ["01001", "s"],
    ["10001", "h"],
    ["00110", "r"],
    ["01100", "d"],
    ["11000", "l"],
    ["00111", "c"],
    ["10100", "u"],
    ["11101", "m"],
    ["01010", "w"],
    ["10010", "f"],
    ["01111", "g"],
    ["01011", "y"],
    ["01110", "p"],
    ["10011", "b"],
    ["10101", "v"],
    ["10110", "j"],
    ["11001", "k"],
    ["11010", "x"],
    ["11100", "q"],
    ["10111", "z"],
    ["11011", "."],
    ["01101", ","],
    ["11110", "KEY_BACKSPACE"],
    ["11111", "KEY_RETURN"],
]

chars = set(v for k, v in keys)
target_chars = {
    " ",
    ",",
    ".",
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "i",
    "j",
    "k",
    "KEY_BACKSPACE",
    "KEY_RETURN",
    "l",
    "m",
    "n",
    "o",
    "p",
    "q",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "y",
    "z",
}

bits = set(k for k, v in keys)
target_bits = {
    "00001",
    "00010",
    "00011",
    "00100",
    "00101",
    "00110",
    "00111",
    "01000",
    "01001",
    "01010",
    "01011",
    "01100",
    "01101",
    "01110",
    "01111",
    "10000",
    "10001",
    "10010",
    "10011",
    "10100",
    "10101",
    "10110",
    "10111",
    "11000",
    "11001",
    "11010",
    "11011",
    "11100",
    "11101",
    "11110",
    "11111",
}

diff = lambda x, y: (x | y) - (x & y)
if diff(chars, target_chars):
    print(diff(chars, target_chars))
    sys.exit(1)
if diff(bits, target_bits):
    print(diff(bits, target_bits))
    sys.exit(1)


# Change endianness.
k = sorted([(x[::-1], y) for x, y in keys])
for pattern, key in k:
    print(f"{key}," if len(key) > 1 else f"'{key}',")
print()

for k, v in sorted((v, k) for k, v in keys):
    print(v, k)
