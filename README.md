Keyyyyyyyys!
============

I made a one-handed, chorded, wireless Bluetooth keyboard so your doctor
can finally take notes while gazing longingly into your eyes rather than
a computer screen.


![A photo of the keyboard](misc/keyboard.jpg)

Usage
-----

Get an ESP32, hook five switches up to its GPIOs, stick it in a case and
you're done. Now you can pair it with your favorite device as a BT
keyboard, learn the 31 key combinations and you're off!

I'm not going to post more instructions than this because who will ever
want to use this thing? Nobody, that's who.
